using System;
using System.Text;

namespace JaHoMusic.YouTube {
	public class VideoLayout {
		public String Playlist { get; internal set; }
		public String Account { get; internal set; }
		public String Title { get; internal set; }
		public String Description { get; internal set; }
		public String Link { get; internal set; }

		public override string ToString ( ) {
			StringBuilder str = new StringBuilder ( );
			str.Append ( "Playlist: " ).AppendLine ( Playlist )
			   .Append ( "Account: " ).AppendLine ( Account )
			   .Append ( "Title: " ).AppendLine ( Title )
			   .Append ( "Link: " ).AppendLine ( Link );

			return str.ToString ( );
		}
	}
}
