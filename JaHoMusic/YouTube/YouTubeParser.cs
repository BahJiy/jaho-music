using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using JaHoMusic.Webhelp;

namespace JaHoMusic.YouTube {
	public class YouTubeParser : Scraper<List<VideoLayout>> {
		#region RegEx
		private Regex RexYouTubeURL = new Regex ( @"https://www.youtube.com/(watch|playlist).*", RegexOptions.Multiline );
		private Regex RexPlaylistItem = new Regex ( @"yt-uix-tile-link.+?href=\\?""\\?(\/watch\?.+?)[&\\].+?""[\n\s]*data-session.*>[\n\s]+(.*?)[\n\s]+?<\/a>", RegexOptions.Multiline );
		private Regex RexPlaylistTitle = new Regex ( @"<h1 class=""pl-header-title"".+>[\n\s]*(.+)[\n\s]*<\/h1>", RegexOptions.Multiline );
		private Regex RexPlaylistLoadMore = new Regex ( @"data-uix-load-more-href=\\?""\\?((.|\n)+?)\\?""", RegexOptions.Multiline );
		private Regex RexVideoAccount = new Regex ( @"yt-user-info"">[\n\s]+?<a href=""\/channel\/.*?""[\n\s]+?data-sessionlink="".*?>[\n\s]*?((.|\n)+?)<\/a>", RegexOptions.Multiline );
		private Regex RexVideoTitle = new Regex ( @"<meta itemprop=""name""[\n\s]+?content=""(.+?)"">", RegexOptions.Multiline );
		private Regex RexVideoDescription = new Regex ( @"<p id=""eow-description""[\n\s]+?class="".*?""[\n\s]*?>[\n\s]?((.|\n)+?)[\n\s]?<\/p>", RegexOptions.Multiline );
		#endregion RegEx
		private string YouTubeURL ( string part ) => $"https://www.youtube.com/{part}&disable_polymer=true";

		public YouTubeParser ( ) { }

		/// <summary>
		/// Parse either a playlist or a video webpage into a VideoLayout struct
		/// </summary>
		/// <param name="url">The url to a playlist or video webpage</param>
		/// <returns>A list of VideoLayouts. If the url points to a video page, the list will contain one item</returns>
		public override async Task<List<VideoLayout>> ParseAsync ( string url ) {
			if ( RexYouTubeURL.Match ( url ).Success ) {
				return await Task<List<VideoLayout>>.Run ( async ( ) => {
					if ( Regex.Match ( url, "playlist" ).Success ) {
						return await ParsePlaylistAsync ( url );
					} else {
						return new List<VideoLayout> ( ) { ParseVideoPageAsync ( url ) };
					}
				} );
			}
			throw new UnknownWebDomainException ( $"Invalid YouTube url {url}" );
		}

		/// <summary>
		/// Parse a video webpage into a VideoLayout
		/// </summary>
		/// <param name="url">The url to a video webpage</param>
		/// <param name="layout">Add to the given layout if there is one</param>
		/// <returns>The VideoLayout info of the video</returns>
		private VideoLayout ParseVideoPageAsync ( string url, VideoLayout layout = null ) {
			layout = layout == null ? new VideoLayout ( ) { Link = url } : layout;
			string pageHTML = ReadPage ( url );

			Match match = null;
			if ( String.IsNullOrWhiteSpace ( layout.Title ) ) {
				if ( ( match = RexVideoTitle.Match ( pageHTML ) ).Success )
					layout.Title = match.Groups[ 1 ].Value;
				else {
					throw new ParsingException ( "Unable to find video's title. Assume invalid video. URL: " + url );
				}
			}
			if ( ( match = RexVideoAccount.Match ( pageHTML, match == null ? 0 : match.Index ) ).Success )
				layout.Account = match.Groups[ 1 ].Value;
			else {
				layout.Account = "N/A";
				throw new ParsingException ( "Unable to find video's account. Assume invalid video. URL: " + url );
			}
			if ( ( match = RexVideoDescription.Match ( pageHTML, match.Index ) ).Success ) {
				layout.Description = match.Groups[ 1 ].Value;
				layout.Description = Regex.Replace ( layout.Description, @"<br\s+\/>", Environment.NewLine );
			} else {
				layout.Description = "N/A";
				throw new ParsingException ( "Unable to find video's description. Assume invalid video. URL: " + url );
			}

			pageHTML = null;

			return layout;
		}

		/// <summary>
		/// Parse a playlist into a list of VideoLayouts. This is the same as
		/// running ParseVideoPage on each video url in the playlist
		/// </summary>
		/// <param name="url">The url to the playlist</param>
		/// <returns>The list of VideoLayout</returns>
		private async Task<List<VideoLayout>> ParsePlaylistAsync ( string url ) {
			List<VideoLayout> layouts = new List<VideoLayout> ( );

			// Get all the playlist's videos
			List<VideoLayout> playlist = await GetPlaylistItemsAsync ( url );

			// Then extract information from them
			foreach ( VideoLayout video in playlist ) {
				try {
					ParseVideoPageAsync ( video.Link, video );
				} catch ( ParsingException e ) {
					Console.WriteLine ( e );
				}
				layouts.Add ( video );
			}
			GC.Collect ( );

			return layouts;
		}

		/// <summary>
		/// Given the URL to a YouTube playlist, extract the playlist's name and video links
		/// </summary>
		/// <param name="url">The URL to a YouTube playlist</param>
		/// <returns>A list of VideoLayouts with Playlist, Title, and Link populated</returns>
		private async Task<List<VideoLayout>> GetPlaylistItemsAsync ( string url ) {
			List<VideoLayout> urls = new List<VideoLayout> ( );

			// Get the initial playlist HTML
			string pageHTML = ReadPage ( url + "&disable_polymer=true" );
			Match title = RexPlaylistTitle.Match ( pageHTML );
			if ( !title.Success ) {
				throw new ParsingException ( "Unable to find playlist title" );
			}
			while ( !String.IsNullOrEmpty ( pageHTML ) ) {
				// Attempt to find the "Load More" button to get all the other
				// videos. Each page contains a max of 100 videos
				Match loadMore = RexPlaylistLoadMore.Match ( pageHTML );
				Task<string> pageData = loadMore.Success ? ReadPageAsync ( YouTubeURL ( loadMore.Groups[ 1 ].Value ) ) : null;

				// Read the page and extract the videos' link
				MatchCollection videoUrls = RexPlaylistItem.Matches ( pageHTML );
				foreach ( Match item in videoUrls ) {
					urls.Add ( new VideoLayout ( ) {
						Playlist = title.Groups[ 1 ].Value,
						Title = item.Groups[ 2 ].Value,
						Link = YouTubeURL ( item.Groups[ 1 ].Value )
					} );
				}

				// Get the next page data if available
				pageHTML = pageData == null ? null : await pageData;
			}
			GC.Collect ( );
			return urls;
		}
	}
}
