using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace JaHoMusic.Webhelp {
	public abstract class Scraper<T> {

		/// <summary>
		/// Parse the webpage into format T
		/// </summary>
		/// <param name="url">The url to the webpage to parse</param>
		/// <returns>The webpage parsed into data format T</returns>
		public abstract Task<T> ParseAsync ( string url );

		/// <summary>
		/// Download the webpage html asynchouriously
		/// </summary>
		/// <param name="url">The url of the webpage</param>
		/// <returns>The url's html data as a string</returns>
		public async Task<string> ReadPageAsync ( string url ) {
			using ( WebClient client = new WebClient ( ) ) {
				return await client.DownloadStringTaskAsync ( url );
			}
		}

		/// <summary>
		/// Download the webpage html
		/// </summary>
		/// <param name="url">The url of the webpage</param>
		/// <returns>The url's html data as a string</returns>
		public string ReadPage ( string url ) {
			using ( WebClient client = new WebClient ( ) ) {
				return client.DownloadString ( url );
			}
		}

		/// <summary>
		/// Save the data to a file in the dump folder asynchronously
		/// </summary>
		/// <param name="name">The name of the file</param>
		/// <param name="data">The content to save</param>
		internal async void SaveAsync ( string name, string data ) {
			await Task.Run ( ( ) => {
				Directory.CreateDirectory ( "dump" );
				using ( StreamWriter writer = new StreamWriter ( "dump/" + name ) ) {
					writer.Write ( data );
				}
			} );
		}

		/// <summary>
		/// Save the data to a file in the dump folder
		/// </summary>
		/// <param name="name">The name of the file</param>
		/// <param name="data">The content to save</param>
		internal void Save ( string name, string data ) {
				Directory.CreateDirectory ( "dump" );
				using ( StreamWriter writer = new StreamWriter ( "dump/" + name ) ) {
					writer.Write ( data );
				}
		}
	}

	/// <summary>
	/// This should be thrown when the URL passed to the parser is unsupported by the parser.
	/// </summary>
	public class UnknownWebDomainException : Exception {
		public UnknownWebDomainException ( ) { }

		public UnknownWebDomainException ( string message ) : base ( message ) { }
	}

	/// <summary>
	/// This should be thrown when the parser is unable to find a required element.
	/// This typically means the webpage is newer than what the parser supports.
	/// </summary>
	public class ParsingException : Exception {
		public ParsingException ( ) { }

		public ParsingException ( string message ) : base ( message ) { }
	}
}
