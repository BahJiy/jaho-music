﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using JaHoMusic.Webhelp;
using JaHoMusic.YouTube;
using TagLib.Mpeg4;
using TagLib;
namespace JaHoMusic {
	public class Program {
		public static async Task Main ( string[ ] args ) {
			String url = "https://www.youtube.com/playlist?list=PLjdQKD94A1T-lTLIQU7N1VL9_CA5SA5p0";
			YouTubeParser youtube = new YouTubeParser ( );
			var result = await youtube.ParseAsync ( url );
			for ( int i = 0; i < result.Count; i++ ) {
				VideoLayout layout = result[ i ];
				Console.WriteLine ( i );
				Console.WriteLine ( layout );
			}
		}
	}
}
